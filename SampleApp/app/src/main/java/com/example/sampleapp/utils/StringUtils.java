package com.example.sampleapp.utils;

import java.util.Locale;

public class StringUtils {
    public String getAmountFormatted(Double amount) {
        return "Rs. " + String.format(Locale.getDefault(), "%.2s", amount) + "/-";
    }
}