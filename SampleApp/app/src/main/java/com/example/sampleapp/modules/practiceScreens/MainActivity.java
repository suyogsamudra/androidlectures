package com.example.sampleapp.modules.practiceScreens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sampleapp.R;

public class MainActivity extends AppCompatActivity {

    private EditText eTxtMobile, eTxtPassword;
    ProgressBar progressBar;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        final Button btnLogin = findViewById(R.id.btnLogin);
        eTxtPassword = findViewById(R.id.eTxtPassword);
        eTxtMobile = findViewById(R.id.eTxtMobile);
        progressBar = findViewById(R.id.progressBar);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:983444052777"));
                startActivity(intent);*/

                btnLogin.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String mobile = eTxtMobile.getText().toString().trim();
                        String password = eTxtPassword.getText().toString().trim();

                        if ((mobile.equals("9834440527") && password.equals("123456")) || (mobile.equals("1234567890") && password.equals("123123"))) {
                            Toast.makeText(context, "Login Successful", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(context, RegistrationActivity.class);
                            startActivity(intent);
                            finish();
                        } else
                            Toast.makeText(context, "Invalid credentials", Toast.LENGTH_SHORT).show();

                        btnLogin.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                }, 3000);
            }
        });
    }
}