package com.example.sampleapp.modules.googleMeet;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sampleapp.R;
import com.google.android.material.tabs.TabLayout;

public class GoogleMeetSampleActivity extends AppCompatActivity {

    private Context context;
    private boolean isMuted = true, isVideoOff = true;
    private ImageView imgVideo, imgAudio;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_meet_sample_activity);
        context = this;

        imgAudio = findViewById(R.id.imgMute);
        imgVideo = findViewById(R.id.imgVideo);

        imgAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMuted) {
                    imgAudio.setImageResource(R.drawable.icon_mic_unmute);
                    Toast.makeText(context, "Mic. On", Toast.LENGTH_SHORT).show();
                } else {
                    imgAudio.setImageResource(R.drawable.icon_mic_mute);
                    Toast.makeText(context, "Mic. off", Toast.LENGTH_SHORT).show();
                }
                isMuted = !isMuted;
            }
        });

        imgVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isVideoOff) {
                    imgVideo.setImageResource(R.drawable.icon_video_on);
                    Toast.makeText(context, "Video On", Toast.LENGTH_SHORT).show();
                } else {
                    imgVideo.setImageResource(R.drawable.icon_video_off);
                    Toast.makeText(context, "Video Off", Toast.LENGTH_SHORT).show();
                }
                isVideoOff = !isVideoOff;
            }
        });

        findViewById(R.id.cardCallDrop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hangUpCall();
            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new SampleListFragment()).commit();

        TabLayout tabLytCall = findViewById(R.id.tabLytCall);

        tabLytCall.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new ParticipantsFragment()).commit();
                        break;
                    case 1:
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, CallInfoFragment.newInstance("This is Chat Fragment")).commit();
                        break;
                    default:
                        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, CallInfoFragment.newInstance("This is Info Fragment")).commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        hangUpCall();
    }

    private void hangUpCall() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("End call").setMessage("Are you sure you want to end the the call now?").setPositiveButton("End Now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialog != null) dialog.dismiss();
                finish();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialog != null) dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}