package com.example.sampleapp.modules.googleMeet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sampleapp.R;

public class CallInfoFragment extends Fragment {

    private static final String ARGUMENT_TITLE = "argTitle";

    public static CallInfoFragment newInstance(String text) {
        CallInfoFragment frag = new CallInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARGUMENT_TITLE, text);
        frag.setArguments(bundle);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.dummy_fragment_layout, container, false);
        TextView txtDummy = rootView.findViewById(R.id.txtDummyText);
        txtDummy.setText(getArguments().getString(ARGUMENT_TITLE));

        return rootView;
    }
}