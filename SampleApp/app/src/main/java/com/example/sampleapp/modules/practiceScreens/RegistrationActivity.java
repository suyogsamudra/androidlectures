package com.example.sampleapp.modules.practiceScreens;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sampleapp.R;

import java.util.ArrayList;
import java.util.List;

public class RegistrationActivity extends AppCompatActivity {

    private Context context;

    EditText eTxtFullName;
    RadioGroup radGrpGender;
    CheckBox chkTerms;
    Button btnSubmit;
    RecyclerView recStudents;
    Spinner spinCountry;
    AlertDialog dialog;

    private List<Integer> studentsList = new ArrayList<>(5);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);
        context = this;

        eTxtFullName = findViewById(R.id.eTxtFullName);
        radGrpGender = findViewById(R.id.radGrpGender);
        chkTerms = findViewById(R.id.chkTerms);
        btnSubmit = findViewById(R.id.btnSubmit);
        spinCountry = findViewById(R.id.spinCountry);
        recStudents = findViewById(R.id.recStudents);

        List<String> countries = new ArrayList<>();
        countries.add("India");
        countries.add("USA");
        countries.add("Japan");

        spinCountry.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, countries));

        eTxtFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validate();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        radGrpGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                validate();
            }
        });

        chkTerms.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                validate();
                studentsList.add(R.drawable.image3);
                StudentsRecAdapter adapter = (StudentsRecAdapter) recStudents.getAdapter();
                if (adapter != null) adapter.updateList(studentsList);
            }
        });

        spinCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                Toast.makeText(context, "Country Selected: " + parent.getItemAtPosition(pos).toString(), Toast.LENGTH_SHORT).show();
                validate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        studentsList.add(R.drawable.image1);
        studentsList.add(R.drawable.image2);
        studentsList.add(R.drawable.image3);
        //studentsList.add(new Student(3, "test"));

        recStudents.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, true));
        recStudents.setAdapter(new StudentsRecAdapter(studentsList));

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(GridLayoutActivity.getStartIntent(context, eTxtFullName.getText().toString(), 25));
            }
        });


        TextView txtFormNote = findViewById(R.id.txtFormNote);
        txtFormNote.setVisibility(View.VISIBLE);
        txtFormNote.setEnabled(true);
        txtFormNote.setText("Hello world");

        radGrpGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                if(id == R.id.radioMale) Toast.makeText(context, "Male", Toast.LENGTH_SHORT).show();

            }
        });

        chkTerms.setChecked(false);
        chkTerms.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void validate() {
        String name = eTxtFullName.getText().toString().trim();
        btnSubmit.setEnabled(!name.equals("") && chkTerms.isChecked());
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Go Back").setMessage(R.string.registration_go_back_desc).setPositiveButton("Go Back", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialog != null) dialog.dismiss();
                finish();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialog != null) dialog.dismiss();
            }
        });

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}