package com.example.sampleapp.modules.googleMeet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sampleapp.R;
import com.example.sampleapp.model.customObjects.CallParticipant;

import java.util.ArrayList;

public class ParticipantsFragment extends Fragment {

    public ParticipantsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.participants_fragment, container, false);
        RecyclerView recParticipants = rootView.findViewById(R.id.recParticipants);

        recParticipants.setLayoutManager(new LinearLayoutManager(getContext()));

        ArrayList<CallParticipant> list = new ArrayList<>();

        list.add(new CallParticipant("Mahesh Jadhav", false, true));
        list.add(new CallParticipant("Suyog Samudra", true, false));
        list.add(new CallParticipant("Gauravi Rajput", false, false));
        list.add(new CallParticipant("Sumitra Saraf", false, true));
        list.add(new CallParticipant("Ajinkya Patil", false, true));

        recParticipants.setAdapter(new ParticipantsAdapter(list));
        return rootView;
    }
}

class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsAdapter.ViewHolder> {

    private ArrayList<CallParticipant> participantsList;

    public ParticipantsAdapter(ArrayList<CallParticipant> participantsList) {
        this.participantsList = participantsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.call_participant_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CallParticipant part = participantsList.get(position);
        holder.txtName.setText(part.fullName);
        if (part.isMuted) holder.cardMicStatus.setVisibility(View.VISIBLE);
        else holder.cardMicStatus.setVisibility(View.GONE);
        if (part.isSelf) holder.imgDetails.setVisibility(View.GONE);
        else holder.imgDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return participantsList.size();
    }

    @SuppressWarnings("InnerClassMayBeStatic")
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName;
        private CardView cardMicStatus;
        private ImageView imgDetails;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            cardMicStatus = itemView.findViewById(R.id.cardMicStatus);
            imgDetails = itemView.findViewById(R.id.imgDetails);
        }
    }
}