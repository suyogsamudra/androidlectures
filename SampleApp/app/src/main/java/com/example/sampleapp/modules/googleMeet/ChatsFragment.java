package com.example.sampleapp.modules.googleMeet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sampleapp.R;

public class ChatsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.dummy_fragment_layout, container, false);
        TextView txtDummy = rootView.findViewById(R.id.txtDummyText);
        txtDummy.setText("This is Chat Fragment");

        return rootView;
    }
}