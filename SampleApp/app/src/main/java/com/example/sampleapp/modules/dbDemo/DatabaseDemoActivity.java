package com.example.sampleapp.modules.dbDemo;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sampleapp.R;
import com.example.sampleapp.business.database.AppDatabase;
import com.example.sampleapp.business.database.User;

import java.util.ArrayList;
import java.util.List;

public class DatabaseDemoActivity extends AppCompatActivity {

    private AppDatabase conn;
    private EditText eTxtFirstName, eTxtLastName;
    private UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.databse_demo_activity);
        Context context = this;

        eTxtFirstName = findViewById(R.id.eTxtFName);
        eTxtLastName = findViewById(R.id.eTxtLName);
        RecyclerView recUsers = findViewById(R.id.recUsers);
        recUsers.setLayoutManager(new LinearLayoutManager(context));
        recUsers.addItemDecoration(new DividerItemDecoration(context, RecyclerView.VERTICAL));

        conn = AppDatabase.getInstance(context);
        adapter = new UserAdapter();

        class DeleteTask extends AsyncTask<Void, Void, Void> {

            private User user;

            public DeleteTask(User user) {
                this.user = user;
            }

            @Override
            protected Void doInBackground(Void... voids) {
                conn.userDao().delete(user);
                adapter.userList = conn.userDao().getAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                adapter.notifyDataSetChanged();
                super.onPostExecute(aVoid);
            }
        }

        adapter.listener = new UserAdapter.ItemClickListener() {
            @Override
            public void onItemClick(final User user) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        new DeleteTask(user).execute();
                    }
                });
            }
        };

        recUsers.setAdapter(adapter);
        class InsertTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                conn.userDao().insertAll(new User(eTxtFirstName.getText().toString().trim(), eTxtLastName.getText().toString().trim()));
                adapter.userList = conn.userDao().getAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                adapter.notifyDataSetChanged();
                super.onPostExecute(aVoid);
            }
        }

        findViewById(R.id.btnAddUser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new InsertTask().execute();
            }
        });
    }
}

class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    List<User> userList = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.db_user_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final User user = userList.get(position);
        holder.txtFullName.setText(String.format("%s %s", user.firstName, user.lastName));
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) listener.onItemClick(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtFullName;
        private ImageView imgDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtFullName = itemView.findViewById(R.id.txtFullName);
            imgDelete = itemView.findViewById(R.id.imgDelete);
        }
    }

    public interface ItemClickListener {
        void onItemClick(User user);
    }

    public ItemClickListener listener;
}