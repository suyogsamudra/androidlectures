package com.example.sampleapp.model.customObjects;

public class CallParticipant {
    public String fullName;
    public Boolean isSelf, isMuted;

    public CallParticipant(String fullName, Boolean isSelf, Boolean isMuted) {
        this.fullName = fullName;
        this.isSelf = isSelf;
        this.isMuted = isMuted;
    }
}

