package com.example.sampleapp.modules.practiceScreens;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sampleapp.R;

import java.util.List;

public class StudentsRecAdapter extends RecyclerView.Adapter<StudentsRecAdapter.ViewHolder> {

    private List<Integer> studentList;

    public StudentsRecAdapter(List<Integer> studentList) {
        this.studentList = studentList;
    }

    public void updateList(List<Integer> list) {
        this.studentList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_cell, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //holder.txtStudentName.setText(studentList.get(position).name);
        holder.imgStudent.setImageResource(studentList.get(position));
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtStudentName;
        ImageView imgStudent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imgStudent = itemView.findViewById(R.id.imgStudent);
            //txtStudentName = itemView.findViewById(R.id.txtStudentName);
        }
    }
}
