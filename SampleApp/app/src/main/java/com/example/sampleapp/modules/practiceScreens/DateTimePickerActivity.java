package com.example.sampleapp.modules.practiceScreens;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.sampleapp.R;

import java.util.Calendar;
import java.util.Locale;

public class DateTimePickerActivity extends AppCompatActivity {

    private Context context;
    TextView txtDate, txtTime;
    int hour = 0, minutes = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.date_time_picker_activity);
        context = this;

        Button btnDatePick, btnTimePick;

        btnDatePick = findViewById(R.id.btnSelectDate);
        btnTimePick = findViewById(R.id.btnSelectTime);

        txtDate = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);

        btnDatePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.YEAR, -18);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        txtDate.setText(String.format(Locale.getDefault(),
                                "%s %02d/%02d/%04d", "You have selected", dayOfMonth, month + 1, year));
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.setCanceledOnTouchOutside(false);
                datePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
                cal.add(Calendar.YEAR, -82);
                datePickerDialog.getDatePicker().setMinDate(cal.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        btnTimePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String meridian;
                        if (hourOfDay > 11) meridian = "PM";
                        else meridian = "AM";
                        int hr;
                        if(hourOfDay > 12) hr = (hourOfDay - 12); else hr = hourOfDay;
                        txtTime.setText(String.format(Locale.getDefault(),
                                "%s %02d:%02d %s", "You have selected", hr , minute, meridian));
                        hour = hourOfDay;
                        minutes = minute;
                    }
                }, hour, minutes, false);
                timePickerDialog.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.primary_menu, menu);
        //menu.add(Menu.NONE, 1010, Menu.NONE, "Test");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.menuProfile)
            Toast.makeText(context, "Profile", Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }
}