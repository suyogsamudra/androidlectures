package com.example.sampleapp.modules.practiceScreens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sampleapp.R;

public class GridLayoutActivity extends AppCompatActivity {

    static final String INTENT_NAME = "name";
    static final String INTENT_AGE = "age";

    public static Intent getStartIntent(Context context, String name, int age) {
        Intent intent = new Intent(context, GridLayoutActivity.class);
        intent.putExtra(INTENT_NAME, name);
        intent.putExtra(INTENT_AGE, age);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_layout_activity);

        String name = getIntent().getStringExtra(INTENT_NAME);
        int age = getIntent().getIntExtra(INTENT_AGE, 10);

        TextView txtName = findViewById(R.id.txtStudentName);
        txtName.setText(": " + name + " " + age + " years");
    }
}